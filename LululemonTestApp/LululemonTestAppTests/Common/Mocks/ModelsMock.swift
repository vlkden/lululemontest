//
//  ModelsMock.swift
//  LululemonTestAppTests
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation

@testable import LululemonTestApp


private enum GarmentName: String, CaseIterable {
    case dress, pant, tShort, coat, underwear
}

extension Garment {
    static func mockWithRandomData() -> Garment {
        let name = GarmentName.allCases.randomElement()?.rawValue ?? GarmentName.dress.rawValue
        return Garment(id: UUID(),
                       name: name,
                       created: Date(),
                       updated: Date())
    }
    
    static func mock(with name: String) -> Garment {
        return Garment(id: UUID(),
                       name: name,
                       created: Date(),
                       updated: Date())
    }
}
