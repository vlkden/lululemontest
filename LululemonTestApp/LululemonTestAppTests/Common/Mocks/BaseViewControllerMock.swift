//
//  BaseViewControllerMock.swift
//  LululemonTestAppTests
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation
import UIKit

@testable import LululemonTestApp

class BaseViewControllerMock: UIViewController, BaseViewControllerProtocol {
    var loadingController: LoadingViewController?
    
    private(set) var showErrorEventReceived = false

    private(set) var showLoadingControllerEventReceived = false
    private(set) var hideLoadingControllerEventReceived = false
    
    private(set) var setTitleEventReceived = false
    
    private(set) var errorMessage: String?
    private(set) var errorTitle: String?
    private(set) var error: NSError?
    private(set) var navBarTitle: String?
    
    func set(title: String) {
        navBarTitle = title
        setTitleEventReceived = true
    }
    
    func show(error: NSError) {
        showErrorEventReceived = true
        self.error = error
    }
    
    func show(message: String, title: String?) {
        showErrorEventReceived = true
        errorTitle = title
        errorMessage = message
    }
    
    func showLoadingController(aboveNavBar: Bool) {
        showLoadingControllerEventReceived = true
    }
    
    func hideLoadingController() {
        hideLoadingControllerEventReceived = true
    }
}
