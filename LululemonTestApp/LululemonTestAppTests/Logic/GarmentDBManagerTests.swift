//
//  GarmentDBManagerTests.swift
//  LululemonTestAppTests
//
//  Created by Denys Volkov on 2021-06-28.
//

import XCTest

@testable import LululemonTestApp

private enum GarmentName: String {
    case dress, pant, tShort, coat, underwear
}

class GarmentDBManagerTests: XCTestCase {
    
    var sut: GarmentDBManagerProtocol!

    override func setUp() {
        super.setUp()
        
        sut = GarmentDBManager(storageType: .inMemory)
    }

    override func tearDown() {
      super.tearDown()
      sut = nil
    }
    
    func testAddGarment() {
        let garment = sut.addGarment(with: GarmentName.dress.rawValue)
        
        XCTAssertNotNil(garment, "Garment should not be nil")
        XCTAssertTrue(garment.name == GarmentName.dress.rawValue)
        XCTAssertNotNil(garment.id, "id should not be nil")
        XCTAssertNotNil(garment.created, "created date should not be nil")
        XCTAssertNotNil(garment.updated, "updated date should not be nil")
    }
    
    func testFetchGarments() {
        let names: [String] = [
            GarmentName.dress.rawValue,
            GarmentName.pant.rawValue,
            GarmentName.tShort.rawValue
        ]
        
        names.forEach { sut.addGarment(with: $0) }
        
        let garments = sut.fetchAll()
        
        XCTAssertNotNil(garments, "Garment should not be nil")
        XCTAssertTrue(garments.count == 3)
    }
    
    func testFetchWithAlpabetticSorting() {
        let names: [String] = [
            GarmentName.dress.rawValue,
            GarmentName.pant.rawValue,
            GarmentName.tShort.rawValue,
            GarmentName.coat.rawValue,
            GarmentName.underwear.rawValue
        ]
        
        names.forEach { sut.addGarment(with: $0) }
        
        let garments = sut.fetch(with: .alphabetic)
        
        XCTAssertNotNil(garments, "Garment should not be nil")
        XCTAssertTrue(garments.count == 5)
        XCTAssertTrue(garments.first?.name == GarmentName.coat.rawValue)
        XCTAssertTrue(garments.last?.name == GarmentName.underwear.rawValue)
    }
    
    func testFetchWithCreatedDateSorting() throws {
        let names: [String] = [
            GarmentName.pant.rawValue,
            GarmentName.dress.rawValue,
            GarmentName.tShort.rawValue,
            GarmentName.coat.rawValue,
            GarmentName.underwear.rawValue
        ]
        
        names.forEach { sut.addGarment(with: $0) }
        
        let garments = sut.fetch(with: .created)
        
        let firstElement = try XCTUnwrap(garments.first)
        let lastElement = try XCTUnwrap(garments.last)
        
        XCTAssertNotNil(garments, "Garment should not be nil")
        XCTAssertTrue(garments.count == 5)
        XCTAssertTrue(firstElement.name == GarmentName.pant.rawValue)
        XCTAssertTrue(lastElement.name == GarmentName.underwear.rawValue)
        XCTAssertTrue(firstElement.created.timeIntervalSince1970 < lastElement.created.timeIntervalSince1970)
    }
    
    func testRemoveGarments() {
        let garment = sut.addGarment(with: GarmentName.dress.rawValue)
        sut.addGarment(with: GarmentName.pant.rawValue)
        
        sut.remove(garment: garment)
        
        let garments = sut.fetchAll()
        
        XCTAssertNotNil(garments, "Garment should not be nil")
        XCTAssertTrue(garments.count == 1)
        XCTAssertTrue(garments.first!.name == GarmentName.pant.rawValue)
    }
}
