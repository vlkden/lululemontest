//
//  GarmentsListPresenterTest.swift
//  LululemonTestAppTests
//
//  Created by Denys Volkov on 2021-06-28.
//

import XCTest

@testable import LululemonTestApp

class GarmentsListPresenterTest: XCTestCase {
    
    var sut: GarmentsListPresenter?
    var delegate: GarmentsListDelegateMock!
    var controller: GarmentsListControllerMock!
    var dbManager: GarmentDBManagerMock!
    
    override func setUp() {
        super.setUp()
        delegate = GarmentsListDelegateMock()
        controller = GarmentsListControllerMock()
        dbManager = GarmentDBManagerMock(storageType: .persistent)
        
        
        sut = GarmentsListPresenter(controller: controller,
                                    delegate: delegate,
                                    dbManager: dbManager)
    }
    
    func testOnViewDidLoad() {
        dbManager.models = [Garment.mockWithRandomData(), Garment.mockWithRandomData()]
        sut?.onViewDidLoad()
        
        XCTAssertTrue(controller.showLoadingControllerEventReceived)
        XCTAssertTrue(controller.showSrotingItemsEventReceived)
        XCTAssertTrue(controller.updatePlaceholderEventReceived)
        XCTAssertTrue(controller.isPlaceholderVisible == false)
        XCTAssertEqual(controller.selectedSortingTypeIndex, 0)
        XCTAssertEqual(controller.rows?.count, 2)
        XCTAssertTrue(controller.setTitleEventReceived)
        XCTAssertEqual(controller.navBarTitle, "Garments list")
    }
    
    func testOnViewDidLoadWithEmptyList() throws {
        sut?.onViewDidLoad()
        
        let placeholderTitle = "Garments list is empty.\n Please tap \"Add garment\" or \"+\" button to add new garment."
        let placeHolderButtonTitle = "Add garment"
        
        let placeholderModel = try XCTUnwrap(controller.placeholderModel)
        
        XCTAssertTrue(controller.showLoadingControllerEventReceived)
        XCTAssertTrue(controller.showSrotingItemsEventReceived)
        XCTAssertTrue(controller.updatePlaceholderEventReceived)
        XCTAssertTrue(controller.isPlaceholderVisible == true)
        XCTAssertTrue(controller.showPlaceholderEventReceived)
        XCTAssertEqual(controller.selectedSortingTypeIndex, 0)
        XCTAssertNil(controller.rows, "Rows should be nil")
        XCTAssertTrue(controller.setTitleEventReceived)
        XCTAssertEqual(controller.navBarTitle, "Garments list")
        XCTAssertEqual(placeholderModel.message, placeholderTitle)
        XCTAssertEqual(placeholderModel.buttonTitle, placeHolderButtonTitle)
    }
    
    func testOnAddGarmentTapped() {
        sut?.onViewDidLoad()
        
        sut?.onCreateGarmentTapped()
        
        XCTAssertTrue(delegate.didSelectCreateGarment)
    }
    
    func testOnSelectSortingType() {
        dbManager.models = [Garment.mockWithRandomData(), Garment.mockWithRandomData()]
        sut?.onViewDidLoad()
        
        sut?.onSortingTypeDidChange(with: 1)
        
        XCTAssertTrue(controller.showLoadingControllerEventReceived)
        XCTAssertEqual(controller.selectedSortingTypeIndex, 1)
    }
}
