//
//  GarmentsListDelegateMock.swift
//  LululemonTestAppTests
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation

@testable import LululemonTestApp

class GarmentsListDelegateMock: GarmentsListPresenterDelegate {
    private(set) var didSelectCreateGarment = false
    
    func presenterDidTapCreateGarment(_ presenter: GarmentsListPresenterProtocol) {
        didSelectCreateGarment = true
    }
}
