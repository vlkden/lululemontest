//
//  GarmentDBManagerMock.swift
//  LululemonTestAppTests
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation

@testable import LululemonTestApp

class GarmentDBManagerMock: GarmentDBManagerProtocol {
    var delegate: GarmentDBManagerDelegate?
    
    var models: Garments = []
    
    private(set) var sortingType: SortingType?
    private(set) var addGarmentEventReceived: Bool = false
    private(set) var removeGarmentEventReceived: Bool = false
    private(set) var fetchAllGarmentEventReceived: Bool = false
    private(set) var fetchGarmentWithSortTypeEventReceived: Bool = false
    
    required init(storageType: StorageType) {}
    
    func addGarment(with name: String) -> Garment {
        addGarmentEventReceived = true
        
        return Garment(id: UUID(), name: name, created: Date(), updated: Date())
    }
    
    func remove(garment: Garment) {
        removeGarmentEventReceived = true
    }
    
    func fetchAll() -> [Garment] {
        fetchAllGarmentEventReceived = true
        
        return models
    }
    
    func fetch(with sortingType: SortingType) -> [Garment] {
        self.fetchGarmentWithSortTypeEventReceived = true
        self.sortingType = sortingType
        
        return models
    }
}
