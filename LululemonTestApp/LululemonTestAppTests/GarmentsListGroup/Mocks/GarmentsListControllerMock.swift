//
//  GarmentsListControllerMock.swift
//  LululemonTestAppTests
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation
import UIKit

@testable import LululemonTestApp

class GarmentsListControllerMock: BaseViewControllerMock, GarmentsListControllerProtocol {
    var presenter: GarmentsListPresenterProtocol?
    
    private(set) var rows: GarmentUIModels?
    private(set) var placeholderModel: GarmentListPlaceholderUIModel?
    private(set) var sortingItems: [String]?
    private(set) var selectedSortingTypeIndex: Int?
    private(set) var isPlaceholderVisible: Bool?
    private(set) var showSrotingItemsEventReceived: Bool = false
    private(set) var showGarmentsEventReceived: Bool = false
    private(set) var updatePlaceholderEventReceived: Bool = false
    private(set) var showPlaceholderEventReceived: Bool = false
    
    
    func show(rows: GarmentUIModels, selectedSortingTypeIndex: Int) {
        self.rows = rows
        self.selectedSortingTypeIndex = selectedSortingTypeIndex
        self.showGarmentsEventReceived = true
        self.isPlaceholderVisible = false
    }
    
    func show(sortingItems: [String], selectedSortingTypeIndex: Int) {
        self.selectedSortingTypeIndex = selectedSortingTypeIndex
        self.sortingItems = sortingItems
        self.showSrotingItemsEventReceived = true
    }
    
    func updatePlaceholder(with model: GarmentListPlaceholderUIModel) {
        placeholderModel = model
        updatePlaceholderEventReceived = true
        isPlaceholderVisible = true
    }
    
    func showPlaceHolder() {
        showPlaceholderEventReceived = true
        isPlaceholderVisible = true
    }
}
