//
//  Garment.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-26.
//

import Foundation

//Garment is DTO model. This layer was added to avoid breaking of dependancy inversion principle, to separate CoreData models and app modules.
struct Garment {
    let id: UUID
    let name: String
    let created: Date
    let updated: Date
}

extension Garment {
    init(with garmentMO: GarmentMO) {
        id = garmentMO.id
        name = garmentMO.name
        created = garmentMO.created
        updated = garmentMO.modifiedDate
    }
}

typealias Garments = [Garment]
