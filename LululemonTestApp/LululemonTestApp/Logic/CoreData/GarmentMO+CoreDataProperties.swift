//
//  Garment+CoreDataProperties.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-26.
//

import Foundation
import CoreData

extension GarmentMO {
    @nonobjc
    public class func fetchRequest() -> NSFetchRequest<GarmentMO> {
        return NSFetchRequest<GarmentMO>(entityName: "GarmentMO")
    }
    
    @NSManaged public var id: UUID
    @NSManaged public var name: String
    @NSManaged public var created: Date
    @NSManaged public var modifiedDate: Date
}
