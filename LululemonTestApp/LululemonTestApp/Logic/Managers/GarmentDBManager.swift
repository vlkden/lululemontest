//
//  GarmentDBManager.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-27.
//

import Foundation
import CoreData

enum StorageType {
  case persistent, inMemory
}

enum SortingType: CaseIterable {
    case alphabetic, created
    
    var sortKey: String {
        switch self {
        case .alphabetic:
            return Constants.CoreData.alphabeticSortKey
        case .created:
            return Constants.CoreData.createdDateSortKey
        }
    }
}

protocol GarmentDBManagerDelegate: AnyObject {
    func dbManagerDidAddGarmet(_ dbManager: GarmentDBManagerProtocol)
}

protocol GarmentDBManagerProtocol {
    var delegate: GarmentDBManagerDelegate? { set get }
    
    init(storageType: StorageType)
    @discardableResult
    func addGarment(with name: String) -> Garment
    func remove(garment: Garment)
    func fetchAll() -> [Garment]
    func fetch(with sortingType: SortingType) -> [Garment]
}

class GarmentDBManager {
    private let persistentContainer: NSPersistentContainer!
    
    weak var delegate: GarmentDBManagerDelegate?
    
    lazy private var backgroundContext: NSManagedObjectContext = {
        return self.persistentContainer.newBackgroundContext()
    }()
    
    required init(storageType: StorageType = .persistent) {
        self.persistentContainer = GarmentDBManager.preparePersistentContainer(with: storageType)
    }
}

extension GarmentDBManager: GarmentDBManagerProtocol {
    @discardableResult
    func addGarment(with name: String) -> Garment {
        let dbGarment = GarmentMO(context: backgroundContext)
        
        dbGarment.id = UUID()
        dbGarment.created = Date()
        dbGarment.name = name
        dbGarment.modifiedDate = dbGarment.created
        
        save()
        
        delegate?.dbManagerDidAddGarmet(self)
        
        return Garment(with: dbGarment)
    }
    
    func remove(garment: Garment) {
        let request: NSFetchRequest<GarmentMO> = GarmentMO.fetchRequest()
        request.predicate = NSPredicate(format: "id == %@", garment.id as CVarArg)
        
        if let results = try? backgroundContext.fetch(request) {
            results.forEach { backgroundContext.delete($0) }
        }
        
        save()
    }
    
    func fetchAll() -> [Garment] {
        let request: NSFetchRequest<GarmentMO> = GarmentMO.fetchRequest()
        if let results = try? persistentContainer.viewContext.fetch(request) {
            return results.map { return Garment(with: $0) }
        }
        
        return [Garment]()
    }
    
    func fetch(with sortingType: SortingType) -> [Garment] {
        let request: NSFetchRequest<GarmentMO> = GarmentMO.fetchRequest()
        let sort = NSSortDescriptor(key: sortingType.sortKey, ascending: true)
        request.sortDescriptors = [sort]
        
        if let results = try? persistentContainer.viewContext.fetch(request) {
            return results.map { return Garment(with: $0) }
        }
        
        return [Garment]()
    }
}

private extension GarmentDBManager {
    func save() {
        if backgroundContext.hasChanges {
            do {
                try backgroundContext.save()
            } catch {
                print("Save error \(error)")
            }
        }
    }
    
    static func preparePersistentContainer(with storageType: StorageType) -> NSPersistentContainer {
        let bundle = Bundle(for: GarmentDBManager.self)
        guard let url = bundle.url(forResource: Constants.CoreData.modelName, withExtension: Constants.CoreData.modelExtension),
              let model = NSManagedObjectModel(contentsOf: url) else {
            fatalError()
        }
        
        let container = NSPersistentContainer(name: Constants.CoreData.modelName,
                                              managedObjectModel: model)
        
        if storageType == StorageType.inMemory {
            let description = NSPersistentStoreDescription()
            description.type = NSInMemoryStoreType
            description.shouldAddStoreAsynchronously = false
            container.persistentStoreDescriptions = [description]
        }
        
        container.loadPersistentStores { [weak container] (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
            container?.viewContext.automaticallyMergesChangesFromParent = true
        }
        
        return container
    }
}
