//
//  GarmentsListController.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation
import UIKit

protocol GarmentsListControllerProtocol: BaseViewControllerProtocol {
    var presenter: GarmentsListPresenterProtocol? { get set }
    
    func show(rows: GarmentUIModels, selectedSortingTypeIndex: Int)
    func show(sortingItems: [String], selectedSortingTypeIndex: Int)
    func updatePlaceholder(with model: GarmentListPlaceholderUIModel)
    func showPlaceHolder()
}

class GarmentsListController: BaseViewController {
    var presenter: GarmentsListPresenterProtocol?
    

    @IBOutlet private weak var placeholderView: UIView!
    private var garmentsPlaceholderView: GarmentsListPlaceholderView = .loadFromXib()!
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var segmentControlPlaceholder: UIView!
    
    private var segmentControl: UISegmentedControl = UISegmentedControl()
    
    private var rows: GarmentUIModels = []
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTableView()
        preparePlaceholderView()
        prepareRightNavBarItem()
        presenter?.onViewDidLoad()
    }
}

extension GarmentsListController: GarmentsListControllerProtocol {
    func show(rows: GarmentUIModels, selectedSortingTypeIndex: Int) {
        self.rows = rows
        
        DispatchQueue.main.async {
            self.placeholderView.isHidden = true
            self.tableView.reloadData()
            self.segmentControl.selectedSegmentIndex = selectedSortingTypeIndex
        }
    }
    
    func show(sortingItems: [String], selectedSortingTypeIndex: Int) {
        prepareSegmentControl(with: sortingItems)
        segmentControl.selectedSegmentIndex = selectedSortingTypeIndex
    }
    
    func updatePlaceholder(with model: GarmentListPlaceholderUIModel) {
        garmentsPlaceholderView.updatePlaceholder(with: model,
                                                  buttonActionClosure: { [weak self] in
                                                    guard let self = self else { return }
                                                    self.presenter?.onCreateGarmentTapped()
                                                  })
    }
    
    func showPlaceHolder() {
        DispatchQueue.main.async {
            self.placeholderView.isHidden = false
        }
    }
}

private extension GarmentsListController {
    // MARK: - Preparations
    func prepareTableView() {
        tableView.register(cells: [GarmentListCell.self])
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
    
    func preparePlaceholderView() {
        placeholderView.addAndFill(garmentsPlaceholderView)
    }
    
    func prepareSegmentControl(with items: [String]) {
        segmentControlPlaceholder.subviews.forEach { $0.removeFromSuperview() }
        
        segmentControl = UISegmentedControl(items: items)
        segmentControlPlaceholder.addAndFill(segmentControl)
        segmentControl.addTarget(self, action: #selector(segmentControlAction), for: .valueChanged)
    }
    
    func prepareRightNavBarItem() {
        let rightNavBarItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add,
                                              target: self,
                                              action: #selector(addGarmentAction))
        
        self.navigationItem.rightBarButtonItem  = rightNavBarItem
    }
    
    @objc func segmentControlAction(_ sender:UISegmentedControl) {
        presenter?.onSortingTypeDidChange(with: sender.selectedSegmentIndex)
    }
    
    @objc func addGarmentAction(_ sender: UIBarButtonItem) {
        presenter?.onCreateGarmentTapped()
    }
}

extension GarmentsListController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = rows[indexPath.row]
        let cell = tableView.dequeueReusableCell(cellClass: GarmentListCell.self)
        
        cell.configure(with: model)
        
        return cell
    }
}
