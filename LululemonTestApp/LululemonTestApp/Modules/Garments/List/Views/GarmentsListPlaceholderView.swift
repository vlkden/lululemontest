//
//  GarmentsListPlaceholderView.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation
import UIKit

private enum PlaceholderConstants {
    static let titleFontSize = CGFloat(16)
    static let buttonBorderColor = UIColor.blue
    static let buttonFontColor = UIColor.blue
    static let borderWidgth = CGFloat(1.0)
    static let cornerRadius = CGFloat(5)
}

class GarmentsListPlaceholderView: UIView {
    @IBOutlet private weak var placeholderTitle: UILabel!
    @IBOutlet private weak var placeholderButton: UIButton!
    
    var buttonActionClosure: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func updatePlaceholder(with model: GarmentListPlaceholderUIModel, buttonActionClosure: (() -> Void)?) {
        self.placeholderTitle.text = model.message
        self.placeholderButton.setTitle(model.buttonTitle, for: .normal)
        self.buttonActionClosure = buttonActionClosure
    }
}

private extension GarmentsListPlaceholderView {
    func setupUI() {
        placeholderTitle.font = UIFont.systemFont(ofSize: PlaceholderConstants.titleFontSize)
        placeholderTitle.lineBreakMode = .byWordWrapping
        placeholderTitle.numberOfLines = 0
        placeholderTitle.textAlignment = .center
        
        placeholderButton.setTitleColor(PlaceholderConstants.buttonFontColor, for: .normal)
        placeholderButton.layer.borderWidth = PlaceholderConstants.borderWidgth
        placeholderButton.layer.borderColor = PlaceholderConstants.buttonBorderColor.cgColor
        placeholderButton.layer.cornerRadius = PlaceholderConstants.cornerRadius
        placeholderButton.layer.masksToBounds = true
        
        placeholderButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }
    
    @objc func buttonAction(_ sender: UIButton) {
        buttonActionClosure?()
    }
}
