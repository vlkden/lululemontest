//
//  GarmentsListPresenter.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation

private enum PresenterConstants {
    static let placeHolderMessageKey = "Garments list is empty.\n Please tap \"Add garment\" or \"+\" button to add new garment."
    static let placeHolderButtonKey = "Add garment"
}

protocol GarmentsListPresenterDelegate: AnyObject {
    /// This method is triggered when "+" button is tapped
    ///
    /// - Parameter presenter: instance of GarmentsListPresenterProtocol
    func presenterDidTapCreateGarment(_ presenter: GarmentsListPresenterProtocol)
}

protocol GarmentsListPresenterProtocol: BasePresenterProtocol {
    /// Called when "+" button is tapped
    func onCreateGarmentTapped()
    
    /// Called when item in segmentControl was tapped
    func onSortingTypeDidChange(with index: Int)
}

class GarmentsListPresenter {
    private weak var delegate: GarmentsListPresenterDelegate?
    private weak var controller: GarmentsListControllerProtocol?
    
    private var dbManager: GarmentDBManagerProtocol
    private let formatter: GarmentsListFormatterProtocol
    
    private var selectedSortingType: SortingType = .alphabetic
    private var sortingTypes: [SortingType] = SortingType.allCases
    private var garments: Garments = []
    
    init(controller: GarmentsListControllerProtocol,
         delegate: GarmentsListPresenterDelegate,
         dbManager: GarmentDBManagerProtocol,
         formatter: GarmentsListFormatterProtocol = GarmentsListFormatter()) {
        self.controller = controller
        self.delegate = delegate
        self.dbManager = dbManager
        self.formatter = formatter
        
        self.controller?.presenter = self
        self.dbManager.delegate = self
    }
}

extension GarmentsListPresenter: GarmentsListPresenterProtocol {
    func onCreateGarmentTapped() {
        delegate?.presenterDidTapCreateGarment(self)
    }
    
    func onViewDidLoad() {
        controller?.showLoadingController(aboveNavBar: true)
        controller?.set(title: Constants.Titles.garmentsListKey.localized)
        controller?.show(sortingItems: formatter.convert(sortingTypes: sortingTypes),
                         selectedSortingTypeIndex: selectedSortingTypeIndex())
        
        let placeHolderModel = GarmentListPlaceholderUIModel(message: PresenterConstants.placeHolderMessageKey.localized,
                                                             buttonTitle: PresenterConstants.placeHolderButtonKey.localized)
        controller?.updatePlaceholder(with: placeHolderModel)
        
        fetchGarments()
    }
    
    func onSortingTypeDidChange(with index: Int) {
        selectedSortingType = sortingTypes[index]
        fetchGarments()
    }
}

private extension GarmentsListPresenter {
    func fetchGarments() {
        garments = dbManager.fetch(with: selectedSortingType)
        controller?.hideLoadingController()
        
        if garments.count > 0 {
            controller?.show(rows: formatter.convert(garment: garments),
                             selectedSortingTypeIndex: selectedSortingTypeIndex())
        } else {
            controller?.showPlaceHolder()
        }
    }
    
    func selectedSortingTypeIndex() -> Int {
        return sortingTypes.firstIndex(where: { $0 == selectedSortingType }) ?? 0
    }
}

extension GarmentsListPresenter: GarmentDBManagerDelegate {
    func dbManagerDidAddGarmet(_ dbManager: GarmentDBManagerProtocol) {
        fetchGarments()
    }
}
