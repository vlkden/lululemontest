//
//  GarmentsListUIModels.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation

struct GarmentListPlaceholderUIModel {
    var message: String
    var buttonTitle: String
}

struct GarmentUIModel {
    let id: UUID
    let name: String
    let createdText: String
}

typealias GarmentUIModels = [GarmentUIModel]
