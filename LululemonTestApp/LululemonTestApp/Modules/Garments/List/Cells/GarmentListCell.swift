//
//  GarmentListCell.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation
import UIKit

private enum CellConstants {
    static let titleFontSize = CGFloat(16)
    static let descriptionFontSize = CGFloat(12)
}

class GarmentListCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var divider: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        assignAccessibilityIds()
        setupAccessibility()
        configureUI()
    }
    
    private func configureUI() {
        titleLabel.font = UIFont.systemFont(ofSize: CellConstants.titleFontSize)
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.numberOfLines = 0
        
        descriptionLabel.font = UIFont.systemFont(ofSize: CellConstants.descriptionFontSize)
        
        divider.backgroundColor = .gray
        
        selectionStyle = .none
    }
}

extension GarmentListCell: ConfigurableView {
    typealias DataType = GarmentUIModel
    
    func configure(with data: DataType) {
        titleLabel.text = data.name
        descriptionLabel.text = data.createdText
        
        updateAccessibilityLabels()
    }
}

extension GarmentListCell: VoiceOverAccessible {
    func setupAccessibility() {
        contentView.isAccessibilityElement = true
        titleLabel.isAccessibilityElement = false
        descriptionLabel.isAccessibilityElement = false

        contentView.accessibilityTraits = .button
    }
    
    func assignAccessibilityIds() {
        titleLabel.accessibilityIdentifier = "GarmentListCell.title"
        descriptionLabel.accessibilityIdentifier = "GarmentListCell.description"
    }
    
    func updateAccessibilityLabels() {
        guard let title = titleLabel.text else {
                contentView.accessibilityLabel = nil
                return
        }
        
        guard let description = descriptionLabel.text else {
            contentView.accessibilityLabel = title
            return
        }
        
        contentView.accessibilityLabel = "\(title) \n\n \(description)"
    }
}
