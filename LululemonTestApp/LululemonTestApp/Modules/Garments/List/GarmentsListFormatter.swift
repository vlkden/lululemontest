//
//  GarmentsListFormatter.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation

private enum FormatterConstants {
    static let dateFormat = "dd MMM yyyy HH:mm:ss"
    
    static let alphaKey = "Alpha"
    static let createdKey = "Created"
}

protocol GarmentsListFormatterProtocol {
    func convert(garment: Garments) -> GarmentUIModels
    func convert(sortingTypes: [SortingType]) -> [String]
}

class GarmentsListFormatter: GarmentsListFormatterProtocol {
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar.current
        dateFormatter.dateFormat = FormatterConstants.dateFormat
        
        return dateFormatter
    }()
    
    func convert(garment: Garments) -> GarmentUIModels {
        return garment.map {
            GarmentUIModel(id: $0.id,
                           name: $0.name,
                           createdText: dateFormatter.string(from: $0.created))
        }
    }
    
    func convert(sortingTypes: [SortingType]) -> [String] {
        let result: [String] = sortingTypes.map { (sortingType) in
            switch sortingType {
            case .alphabetic: return FormatterConstants.alphaKey.localized
            case .created: return FormatterConstants.createdKey.localized
            }
        }
        
        return result
    }
}

