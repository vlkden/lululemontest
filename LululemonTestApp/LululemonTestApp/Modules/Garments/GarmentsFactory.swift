//
//  GarmentsFactory.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation

protocol GarmentsFactoryProtocol {
    func createGarmentsListModule(delegate: GarmentsListPresenterDelegate,
                                  dbManager: GarmentDBManagerProtocol) -> GarmentsListControllerProtocol
    func createAddGarmentModule(delegate: AddGarmentPresenterDelegate,
                                dbManager: GarmentDBManagerProtocol) -> AddGarmentControllerProtocol
}

class GarmentsFactory: GarmentsFactoryProtocol {
    func createGarmentsListModule(delegate: GarmentsListPresenterDelegate,
                                  dbManager: GarmentDBManagerProtocol) -> GarmentsListControllerProtocol {
        let controller = GarmentsListController(loadType: .xib)
        let _ = GarmentsListPresenter(controller: controller,
                                      delegate: delegate,
                                      dbManager: dbManager)
        
        return controller
    }
    
    func createAddGarmentModule(delegate: AddGarmentPresenterDelegate,
                                dbManager: GarmentDBManagerProtocol) -> AddGarmentControllerProtocol {
        let controller = AddGarmentController(loadType: .xib)
        let _ = AddGarmentPresenter(controller: controller,
                                    delegate: delegate,
                                    dbManager: dbManager)
        
        return controller
    }
}
