//
//  GarmentsFlowRouter.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation
import UIKit

class GarmentsFlowRouter {
    private let navController: UINavigationController
    private let factory: GarmentsFactoryProtocol
    private let dbManager: GarmentDBManagerProtocol
    
    init(navController: UINavigationController,
         factory: GarmentsFactoryProtocol = GarmentsFactory(),
         dbManager: GarmentDBManagerProtocol) {
        self.navController = navController
        self.factory = factory
        self.dbManager = dbManager
        
        let controller = factory.createGarmentsListModule(delegate: self, dbManager: dbManager)
        
        navController.viewControllers = [controller]
    }
}

extension GarmentsFlowRouter: GarmentsListPresenterDelegate {
    func presenterDidTapCreateGarment(_ presenter: GarmentsListPresenterProtocol) {
        let controller = factory.createAddGarmentModule(delegate: self, dbManager: dbManager)
        
        navController.pushViewController(controller, animated: true)
    }
}

extension GarmentsFlowRouter: AddGarmentPresenterDelegate {
    func presenter(_ presenter: AddGarmentPresenterProtocol) {
        navController.popViewController(animated: true)
    }
}
