//
//  AddGarmentPresenter.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation

private enum PresenterConstants {
    static let titleKey = "Garment Name:"
}

protocol AddGarmentPresenterDelegate: AnyObject {
    /// This method is triggered when "save" button is tapped and garment was added to DB
    ///
    /// - Parameter presenter: instance of AddGarmentPresenterProtocol
    func presenter(_ presenter: AddGarmentPresenterProtocol)
}

protocol AddGarmentPresenterProtocol: BasePresenterProtocol {
    /// Called when "save" button is tapped
    func onSaveGarmentTapped()
    
    /// Called when text is modified in the textField
    func onTextDidChange(text: String)
}

class AddGarmentPresenter {
    private weak var delegate: AddGarmentPresenterDelegate?
    private weak var controller: AddGarmentControllerProtocol?
    
    private let dbManager: GarmentDBManagerProtocol
    
    private var garmentName = ""
    
    init(controller: AddGarmentControllerProtocol,
         delegate: AddGarmentPresenterDelegate,
         dbManager: GarmentDBManagerProtocol) {
        self.controller = controller
        self.delegate = delegate
        self.dbManager = dbManager
        
        self.controller?.presenter = self
    }
}

extension AddGarmentPresenter: AddGarmentPresenterProtocol {
    func onSaveGarmentTapped() {
        if !garmentName.isEmpty {
            dbManager.addGarment(with: garmentName)
            delegate?.presenter(self)
        }
    }
    
    func onTextDidChange(text: String) {
        garmentName = text
        controller?.saveButton(isEnabled: !garmentName.isEmpty)
    }
    
    func onViewDidLoad() {
        controller?.saveButton(isEnabled: false)
        controller?.update(title: PresenterConstants.titleKey.localized)
    }
}
