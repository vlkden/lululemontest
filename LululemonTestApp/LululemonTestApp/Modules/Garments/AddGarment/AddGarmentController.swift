//
//  AddGarmentController.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation
import UIKit

private enum ControllerConstants {
    static let titleFontSize = CGFloat(16)
    static let textFieldFontSize = CGFloat(16)
    static let textFieldPlaceholderKey = "Enter the garment name"
}

protocol AddGarmentControllerProtocol: BaseViewControllerProtocol {
    var presenter: AddGarmentPresenterProtocol? { get set }
    
    func update(title: String)
    func saveButton(isEnabled: Bool)
}

class AddGarmentController: BaseViewController {
    var presenter: AddGarmentPresenterProtocol?
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var textField: UITextField!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareRightNavBarItem()
        prepareTitleLabel()
        prepareTextField()
        presenter?.onViewDidLoad()
    }
}

extension AddGarmentController: AddGarmentControllerProtocol {
    func update(title: String) {
        titleLabel.text = title
    }
    
    func saveButton(isEnabled: Bool) {
        navigationItem.rightBarButtonItem?.isEnabled = isEnabled
    }
}

private extension AddGarmentController {
    // MARK: - Preparations
    func prepareRightNavBarItem() {
        let rightNavBarItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.save,
                                              target: self,
                                              action: #selector(saveGarmentAction))
        
        navigationItem.rightBarButtonItem  = rightNavBarItem
    }
    
    func prepareTitleLabel() {
        titleLabel.font = UIFont.systemFont(ofSize: ControllerConstants.titleFontSize)
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.numberOfLines = 0
    }
    
    func prepareTextField() {
        textField.font = UIFont.systemFont(ofSize: ControllerConstants.textFieldFontSize)
        textField.placeholder = ControllerConstants.textFieldPlaceholderKey.localized
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    @objc func saveGarmentAction(_ sender: UIBarButtonItem) {
        presenter?.onSaveGarmentTapped()
    }
    
    @objc func textFieldDidChange(_ sender: UITextField) {
        presenter?.onTextDidChange(text: sender.text ?? "")
    }
}
