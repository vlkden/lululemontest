//
//  Constants.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-26.
//

import Foundation

enum Constants {
    enum CoreData {
        static let modelName = "Model"
        static let modelExtension = "momd"
        
        static let alphabeticSortKey = "name"
        static let createdDateSortKey = "created"
    }
    
    enum DateFormats {
        static let main = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
    
    enum Titles {
        static let garmentsListKey = "Garments list"
        static let newGarmentKey = "Add garment"
    }
}
