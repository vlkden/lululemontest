//
//  UIExtensions.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-24.
//

import UIKit

extension UIView {
    func addAndFill(_ subview: UIView) {
        let attributes: [NSLayoutConstraint.Attribute] = [.top, .bottom, .left, .right]
        let constraints = attributes.map {
            NSLayoutConstraint(item: subview,
                               attribute: $0,
                               relatedBy: .equal,
                               toItem: self,
                               attribute: $0,
                               multiplier: 1,
                               constant: 0)
        }
        
        subview.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(subview)
        self.addConstraints(constraints)
    }
    
    /// This method will load view from xib, with provided Type.
    ///
    /// - Returns: View's type
    class func loadFromXib<T: UIView>(with owner: Any? = nil) -> T? {
        guard let results = Bundle(for: self).loadNibNamed(self.className, owner: owner, options: nil) else { return nil }

        for object in results {
            guard let view = object as? T else { continue }
            return view
        }
        return nil
    }
}

extension UIViewController {
    func add(childViewController: UIViewController, to placeholder: UIView) {
        childViewController.willMove(toParent: self)
        self.addChild(childViewController)
        
        childViewController.view.frame = placeholder.bounds
        placeholder.addSubview(childViewController.view)
        placeholder.backgroundColor = .clear
        
        childViewController.didMove(toParent: self)
    }
    
    func embed(childViewController: UIViewController, to placeholder: UIView) {
        childViewController.willMove(toParent: self)
        self.addChild(childViewController)
        
        placeholder.addAndFill(childViewController.view)
        placeholder.backgroundColor = .clear
        
        childViewController.didMove(toParent: self)
    }
    
    func removeFromParentController() {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
        self.didMove(toParent: nil)
    }
    
    func hideBackBtnTitle() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    func showPopup(title: String, text: String) {
        let alertController = UIAlertController(title: title,
                                                message: text,
                                                preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension UITableView {
    func reloadWithoutAnimation() {
        self.layer.removeAllAnimations()
        reloadData()
        layoutIfNeeded()
    }
    
    func register(cells clsArray: [UITableViewCell.Type]) {
        for cls in clsArray {
            register(cellClass: cls)
        }
    }
    
    func register<T>(cellClass cls: T.Type) {
        let nibName = String(describing: cls)
        register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
    }
    
    func register(cellTypes cls: [UITableViewCell.Type]) {
        cls.forEach { self.register(cellType: $0 )}
    }
    
    func register<T: UITableViewCell>(cellType cls: T.Type) {
        let cellClass = String(describing: cls)
        register(cls, forCellReuseIdentifier: cellClass)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(cellClass cls: T.Type, for indexPath: IndexPath) -> T {
        let nibName = String(describing: cls)
        return dequeueReusableCell(withIdentifier: nibName, for: indexPath) as! T
    }
    
    func dequeueReusableCell<T: UITableViewCell>(cellClass cls: T.Type) -> T {
        let nibName = String(describing: cls)
        return dequeueReusableCell(withIdentifier: nibName) as! T
    }
    
    func dequeueReusableCellType<T: UITableViewCell>(cellClass cls: T.Type) -> T {
        return dequeueReusableCell(cellClass: cls)
    }
}

extension UIImageView {
    public func image(with urlString: String, placeHolderImage: String?) {
        func setImage(image: UIImage?) {
            DispatchQueue.main.async {
                self.image = image
            }
        }
        
        if let imageName = placeHolderImage,
            let image = UIImage(named: imageName) {
            setImage(image: image)
        }
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        let theTask = URLSession.shared.dataTask(with: url) {
            data, response, error in
            if let response = data {
                setImage(image: UIImage(data: response))
            }
        }
        theTask.resume()
    }
}
