//
//  Extensions.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-26.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func trimmHTMLTags() -> String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
}

extension NSObject {
    class var className: String {
        return String(describing: self)
    }
}
