//
//  AppRouter.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-28.
//

import Foundation
import UIKit

class AppRouter {
    private let navController: UINavigationController
    private var garmentFlowRouter: GarmentsFlowRouter?
    
    init(navController: UINavigationController) {
        self.navController = navController
        showInitialFlow()
    }
    
    private func showInitialFlow() {
        garmentFlowRouter = GarmentsFlowRouter(navController: navController,
                                               dbManager: GarmentDBManager())
    }
}
