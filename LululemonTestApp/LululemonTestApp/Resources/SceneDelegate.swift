//
//  SceneDelegate.swift
//  LululemonTestApp
//
//  Created by Denys Volkov on 2021-06-23.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var appRouter: AppRouter?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let scene = (scene as? UIWindowScene) else { return }
        let navigationController = BaseNavigationController()
        
        window = UIWindow(windowScene: scene)
        window!.rootViewController = navigationController
        window!.makeKeyAndVisible()
        
        appRouter = AppRouter(navController: navigationController)
    }
}

